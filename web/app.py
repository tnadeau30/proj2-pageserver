from flask import Flask, render_template, request


app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/<name>")
def page(name):
    url_path = request.path
    if url_path.count("//") or url_path.count("~") or url_path.count(".."):
        return render_template("403.html"), 403
    if url_path.endswith(".html") or url_path.endswith(".css"):
        #If file exists, return template
        try:
            return render_template(str(name))
        except:
            return render_template('404.html'), 404 # If after everything, and nothing is ofund, return 404"""
    return render_template('404.html'), 404 # If after everything, and nothing is ofund, return 404

@app.route("/they/<name>")
def folder(name):
    url_path = request.path
    if url_path.count("//") or url_path.count("~") or url_path.count(".."):
        return render_template("403.html"), 403
    if url_path.endswith(".html") or url_path.endswith(".css"):
        #If file exists, return template
        try:
            return render_template(str(name))
        except:
                return render_template('404.html'), 404 # If after everything, and nothing is ofund, return 404"""
    return render_template('404.html'), 404 # If after everything, and nothing is ofund, return 404"""



if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')